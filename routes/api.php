<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('group_city', 'GroupCityController');
Route::apiResource('city', 'CityController');
Route::apiResource('marketing_campaign', 'MarketingCampaignController');
Route::apiResource('product', 'ProductController');

Route::post('marketing_campaign/{idMarketingCampaign}/add_product/{idProduct}','MarketingCampaignController@add_product');
