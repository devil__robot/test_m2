<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'group_city_id',
    ];

    protected $date = [
        'deleted_at',
    ];


    public function groupCity(){
        return $this->belongsTo(GroupCity::class);
    }


    public function my_rules(){
        /*
        resolvendo o problema do unique ao dá Update
        unique:city,name,'.$this->id.'
        1º parâmetro -> nome da tabela
        2º parâmetro -> nome da coluna
        3º parâmetro -> id que será desconsiderado na pesquisa
        */
        return [
            'group_city_id' => 'exists:group_cities,id'
        ];
    }

    public function my_feedback(){
        return [
            'exists' => 'Escolha uma opção válida'
        ];
    }
}
