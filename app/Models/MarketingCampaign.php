<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketingCampaign extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'name',
        'description',
        'discount',
        'group_city_id',
    ];

    protected $date = [
        'deleted_at',
    ];

    public function groupCity(){
        return $this->belongsTo(GroupCity::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }


    public function my_rules(){
        /*
        resolvendo o problema do unique ao dá Update
        unique:city,name,'.$this->id.'
        1º parâmetro -> nome da tabela
        2º parâmetro -> nome da coluna
        3º parâmetro -> id que será desconsiderado na pesquisa
        */
        return [
            'group_city_id' => 'unique:marketing_campaigns,group_city_id,'.$this->id.'|exists:group_cities,id',
        ];
    }

    public function my_feedback(){
        return [
            'unique' => 'Cada grupo só pode possuir 1 Campanha Ativa',
            'exists' => 'Escolha um valor válido'
        ];
    }
}
