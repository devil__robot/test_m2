<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupCity extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
    ];

    protected $date = [
        'deleted_at',
    ];


    public function city(){
        return $this->hasMany(City::class);
    }


    public function my_rules(){
        /*
        resolvendo o problema do unique ao dá Update
        unique:city,name,'.$this->id.'
        1º parâmetro -> nome da tabela
        2º parâmetro -> nome da coluna
        3º parâmetro -> id que será desconsiderado na pesquisa
        */
        return [
            'name' => 'min:3'
        ];
    }

    public function my_feedback(){
        return [
            'min' => 'O nome precisa ter ao menos 3 caractéres.'
        ];
    }
}
