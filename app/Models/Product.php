<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'price',
    ];

    protected $date = [
        'deleted_at',
    ];


    public function marketingCampaigns(){
        return $this->belongsToMany(MarketingCampaign::class);
    }
}
