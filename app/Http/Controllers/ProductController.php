<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

/**
 * @group Produto
 *
 * API endpoints para utilização do Produto
 */


class ProductController extends Controller
{

    public function __construct(Product $product){
        $this->product = $product;

    }
    /**
     * Lista todos os Produtos Cadastrados.
     * Não é necessário passar nenhum parâmetro.
     * Retornará uma lista de Produtos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->all();
        return response()->json($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Armazena um Produto no Banco de Dados
     * @bodyParam   name    string  required    Nome do Produto.      Example: Produto 1
     * @bodyParam   description    text  required    Descrição do Produto.   Example: Produto de ótima qualidade
     * @bodyParam   price    float  required    Preço do Produto.   Example: 30

     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = $this->product->create($request->all());
        return response()->json($product, 200);
    }

    /**
     * Exibe um Produto do Banco de Dados.
     * É necessário passar id de Produto válido
     * Retorna o Produto específico
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->find($id);
        if ($product === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        return response()->json($product, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Atualiza 1 Produto
     * É necessário passar id de Produto Válido
     * @bodyParam   name    string  required    Nome do Produto.      Example: Produto 1
     * @bodyParam   description    text  required    Descrição do Produto.   Example: Produto de ótima qualidade
     * @bodyParam   price    float  required    Preço do Produto.   Example: 30
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = $this->product->find($id);
        if($product === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }         
               
        
        $product->update($request->all());
        return response()->json($product, 200);
    }

    /**
     * Remove 1 Produto
     * É necessário passar id de Produto Válido
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);
        if($product === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }        
        $product->delete();
        return response()->json(['msg' => 'Removido com sucesso!'], 200);
    }
}
