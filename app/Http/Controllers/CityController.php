<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

/**
 * @group Cidade
 *
 * API endpoints para utilização das Cidades
 */

class CityController extends Controller
{

    public function __construct(City $city){
        $this->city = $city;
    }

    /**
     * Lista todos as Cidade Cadastrados.
     * Não é necessário passar nenhum parâmetro.
     * Retornará uma lista de Cidades.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = $this->city->all();
        return response()->json($cities, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Armazena 1 Cidade no Banco de Dados.
     * @bodyParam   name    string  required    Nome da Cidade.      Example: Recife.
     * @bodyParam   description    text  required    Descrição da Cidades.   Example: Cidade quente e litorânea.
     * @bodyParam   group_city_id    Integer  required|exists    Foreign Key  (Id) do Grupo de Cidades.   Example: 1.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate($this->city->my_rules(), $this->city->my_feedback());
        $city = $this->city->create($request->all());
        return response()->json($city, 201);
    }

    /**
     * Exibe 1 Cidade do Banco de Dados.
     * É necessário passar id de 1 Cidade válido.
     * Retorna uma Cidade Específica.
     * Retorna os Grupos de Cidade que a cidade faz parte.
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = $this->city->with('groupCity')->find($id);
        if ($city === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        return response()->json($city, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Atualiza 1 Cidade
     * É necessário passar id de Cidade Válido.
     * @bodyParam   name    string  required    Nome da Cidade.      Example: Recife.
     * @bodyParam   description    text  required    Descrição da Cidades.   Example: Cidade quente e litorânea.
     * @bodyParam   group_city_id    Integer  required|exists    Foreign Key  (Id) do Grupo de Cidades.   Example: 1.
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                
        $request->validate($this->city->my_rules(), $this->city->my_feedback());        
        //$city->update($request->all());
        $city = $this->city->find($id);
        if($city === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        
        $city->update($request->all());
        return response()->json($city, 200);
    }

    /**
     * Remove 1 Cidade.
     * É necessário passar id de Cidades Válido.
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = $this->city->find($id);
        if($city === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }        
        $city->delete();
        return response()->json(['msg' => 'Removido com sucesso!'], 200);
    }
}
