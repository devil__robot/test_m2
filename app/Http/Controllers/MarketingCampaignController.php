<?php

namespace App\Http\Controllers;

use App\Models\MarketingCampaign;
use App\Models\Product;
use Illuminate\Http\Request;

/**
 * @group Campanha de Marketing
 *
 * API endpoints para utilização da Campanha de Marketing.
 */


class MarketingCampaignController extends Controller
{

    public function __construct(MarketingCampaign $marketingCampaign){
        $this->marketingCampaign = $marketingCampaign;
    }
    /**
     * Lista todos as Campanhas de Marketign Cadastradas.
     * Não é necessário passar nenhum parâmetro.
     * Retornará uma lista de Campanhas de Marketign.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marketingCampaigns = $this->marketingCampaign->all();
        return response()->json($marketingCampaigns, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Armazena uma Campanha de Marketing no Banco de Dados.
     * @bodyParam   name    string  required    Nome da Campanha de Marketing.      Example: Campanha I.
     * @bodyParam   description    text  required    Descrição da Campanha de Marketing.   Example: Campanha para promover Produto 1.
     * @bodyParam   group_city_id    Integer  required|exists    Foreign Key  (Id) do Grupo de Cidades.   Example: 1.
     * @bodyParam   discount    float  required    Desconto dados aos Produtos desta Campanha.   Example: 130.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->marketingCampaign->my_rules(), $this->marketingCampaign->my_feedback());
        $marketingCampaign = $this->marketingCampaign->create($request->all());
        return response()->json($marketingCampaign, 200);
    }

    /**
     * Exibe uma Campanha de Marketing do Banco de Dados.
     * É necessário passar id de uma Campanha de Marketing válido.
     * Retorna uma Campanha de Marketing Específica.
     * Retorna os Grupos de Cidade da Campanha de Marketing.
     * Retorna os Produtos da Campanha de Marketing.
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $marketingCampaign = $this->marketingCampaign->with('groupCity')->with('products')->find($id);
        if ($marketingCampaign === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        return response()->json($marketingCampaign, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MarketingCampaign  $marketingCampaign
     * @return \Illuminate\Http\Response
     */
    public function edit(MarketingCampaign $marketingCampaign)
    {
        //
    }

    /**
     * Atualiza 1 Campanha de Marketing.
     * É necessário passar id de Marketing Válido.
     * @bodyParam   name    string  required    Nome da Campanha de Marketing.      Example: Campanha I.
     * @bodyParam   description    text  required    Descrição da Campanha de Marketing.   Example: Campanha para promover Produto 1.
     * @bodyParam   group_city_id    Integer  required|exists    Foreign Key  (Id) do Grupo de Cidades.   Example: 1.
     * @bodyParam   discount    float  required    Desconto dados aos Produtos desta Campanha.   Example: 130.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $marketingCampaign = $this->marketingCampaign->find($id);
        if($marketingCampaign === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        $request->validate($marketingCampaign->my_rules(), $marketingCampaign->my_feedback());        
        
        
        
        $marketingCampaign->update($request->all());
        return response()->json($marketingCampaign, 200);
    }

    /**
     * Remove 1 Campanha de Marketing.
     * É necessário passar id de Produto Válido.
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marketingCampaign = $this->marketingCampaign->find($id);
        if($marketingCampaign === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }        
        $marketingCampaign->delete();
        return response()->json(['msg' => 'Removido com sucesso!'], 200);
    }



    /**
     * Adiciona 1 Produto a 1 Campanha de Marketing.
     * É necessário passar id de Campanha de Marketing Válido.
     * É necessário passar id de Produto Válido.
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function add_product(Request $request, $idMarketingCampaign, $idProduct){
        $marketingCampaign = $this->marketingCampaign->find($idMarketingCampaign);
        //dd($marketingCampaign);
        $product = Product::find($idProduct);
        //dd($product);
        if($marketingCampaign === null || $product === null ){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        $marketingCampaign->products()->attach($product->id);
        return response()->json(["msg" => "Produto adicionado com sucesso a Campanha: ".$marketingCampaign->name], 200); 

    }
}
