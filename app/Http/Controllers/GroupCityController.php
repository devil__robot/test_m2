<?php

namespace App\Http\Controllers;

use App\Models\GroupCity;
use Illuminate\Http\Request;

/**
 * @group Grupo de Cidades
 *
 * API endpoints para utilização dos Grupos de Cidades.
 */

 class GroupCityController extends Controller
{

    public function __construct(GroupCity $groupCity){
        $this->groupCity = $groupCity;

    }
    /**
     * Lista todos os Grupos de Cidade Cadastrados.
     * Não é necessário passar nenhum parâmetro.
     * Retornará uma lista de Grupos de Cidade.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupCities = $this->groupCity->all();
        return response()->json($groupCities, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Armazena 1 Grupo de Cidades no Banco de Dados.
     * @bodyParam   name    string  required    Nome do Grupo de Cidade.      Example: Grupo 1.
     * @bodyParam   description    text  required    Descrição do Grupo de Cidades.   Example: Grupo para as cidades: São Paulo e Recife.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate($this->groupCity->my_rules(), $this->groupCity->my_feedback());
        $groupCity = $this->groupCity->create($request->all());
        return response()->json($groupCity, 200);
    }

    /**
     * Exibe 1 Grupo de Cidades do Banco de Dados.
     * É necessário passar id de Produto válido.
     * Retorna o Grupo de Cidades específico.
     * Retorna as Cidades que fazem parte deste Grupo.
     * 
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $groupCity = $this->groupCity->with('city')->find($id);
        if ($groupCity === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        return response()->json($groupCity, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GroupCity  $groupCity
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupCity $groupCity)
    {
        //
    }

    /**
     * Atualiza 1 Grupo de Cidades
     * É necessário passar id de Grupos de Cidade Válido.
     * @bodyParam   name    string  required    Nome do Grupo de Cidade.      Example: Grupo 1.
     * @bodyParam   description    text  required    Descrição do Grupo de Cidades.   Example: Grupo para as cidades: São Paulo e Recife.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate($this->groupCity->my_rules(), $this->groupCity->my_feedback());        
        //$city->update($request->all());
        $groupCity = $this->groupCity->find($id);
        if($groupCity === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }
        
        $groupCity->update($request->all());
        return response()->json($groupCity, 200);
    }

    /**
     * Remove 1 Grupo de Cidades.
     * É necessário passar id de Grupo de Cidades Válido.
     *
     * @param  Integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupCity = $this->groupCity->find($id);
        if($groupCity === null){
            return response()->json(["msg" => "Registro não encontrado"], 404);
        }        
        $groupCity->delete();
        return response()->json(['msg' => 'Removido com sucesso!'], 200);
    }
}
